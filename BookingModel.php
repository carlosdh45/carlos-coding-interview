<?php

namespace Src\models;

class BookingModel {

	private $bookingData;
	private $helper;

public function createBooking ($data) {
		$booking = $this->getBookings();
		$data['id'] = end($booking)['id'] + 1;
		$booking[] = $data;
		$this->helper->putJson($booking, 'bookings');

		return $data;
	}

public function createBookingForNewClient($booking, $dogs) {
    $clientId = $this->createNewClient($booking['client']);
    $booking['clientid'] = $clientId;

    $this->helper->putJson($booking, 'bookings');

    return $booking;
}

private function createNewClient($client) {
    $clientModel = new ClientModel();
    $newClient = $clientModel->createClient($client);

    return $newClient['id'];
}


	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}


}