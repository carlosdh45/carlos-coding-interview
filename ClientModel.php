<?php

namespace Src\models;

use Src\helpers\Helpers;

class ClientModel {

	private $clientData;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . DIRECTORY_SEPARATOR . '../scripts/clients.json');
		$this->clientData = json_decode($string, true);
	}

	public function getClients() {
		return $this->clientData;
	}

	public function createClient($data) {
		$clients = $this->getClients();

		$data['id'] = end($clients)['id'] + 1;
		$clients[] = $data;

		$this->helper->putJson($clients, 'clients');

		return $data;
	}



	public function softDeleteClient($clientId) {
    $clients = $this->getClients();

    foreach ($clients as &$client) {
        if ($client['id'] == $clientId) {
            $client['deleted'] = true;
            break;
        }
    }

    $this->helper->putJson($clients, 'clients');
}


public function getAverageDogAge($clientId) {
    $dogs = $this->getDogsByClientId($clientId);

    if (empty($dogs)) {
        return 0;
    }

    $totalAge = 0;
    $dogCount = count($dogs);

    foreach ($dogs as $dog) {
        $totalAge += $dog['age'];
    }

    return $totalAge / $dogCount;
}
	public function isEmailValid($email) {
    $pattern = '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/';
    return preg_match($pattern, $email) === 1;
}

	public function createBooking($data) {
    $bookings = $this->getBookings();
    $data['id'] = end($bookings)['id'] + 1;
    $bookings[] = $data;
    $this->helper->putJson($bookings, 'bookings');

    return $data;
}


//Unique email function

public function isEmailUnique($email) {
    $clients = $this->getClients();

    foreach ($clients as $client) {
        if ($client['email'] == $email) {
            return false; 
        }
    }

    return true; 

	}

	public function updateClient($data) {
		$updateClient = [];
		$clients = $this->getClients();
		foreach ($clients as $key => $client) {
			if ($client['id'] == $data['id']) {
				$clients[$key] = $updateClient = array_merge($client, $data);
			}
		}

		$this->helper->putJson($clients, 'clients');

		return $updateClient;
	}

	public function getClientById($id) {
		$clients = $this->getClients();
		foreach ($clients as $client) {
			if ($client['id'] == $id) {
				return $client;
			}
		}
		return null;
	}
}